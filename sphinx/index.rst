
Welcome to  the Duelpy documentation!
=====================================
Open-Source Python package for preference-based multi-armed Bandits algorithms.

For an overview of the problem and algorithms, see :cite:`bengs2021preference`.

.. toctree::
   :maxdepth: 4
   :caption: Contents:

   installation
   examples
   autoapi/index
   release
   glossary
   references


Indices and tables
==================

* :ref:`genindex`

Fork this project
==================

* https://gitlab.com/duelpy/duelpy